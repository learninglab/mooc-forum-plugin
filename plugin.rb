# name: discourse-custom-email-template
# about: An plugin to override default Discourse email template
# version: 0.1
# authors: Benoit Rospars (benoit.rospars@inria.fr)
# url: https://gitlab.inria.fr/learninglab/mooc-forum-plugin

after_initialize {

  ::ActionMailer::Base.prepend_view_path File.expand_path("../custom_views", __FILE__)

}
